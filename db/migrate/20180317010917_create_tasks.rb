class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :title, limit: 255, null: false, default: ''
      t.text :description
      t.integer :status, limit: 1, null: false, default: 0
      t.integer :priority, limit: 1, null:false, default: 0, index: true
      t.integer :user_id, null: false, default: 0
      t.index [ :user_id, :status, :priority ]
      t.timestamps
    end
  end
end
