class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :first_name, limit: 60, null: false, default: ''
      t.string :last_name, limit: 60, null: false, default: ''
      t.string :email, limit: 200, null: false, default: ''
      t.string :password_digest, limit: 255, null: false, default: ''
      t.timestamps
    end
  end
end
