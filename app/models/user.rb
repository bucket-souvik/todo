class User < ApplicationRecord
  has_secure_password

  has_many :tasks, inverse_of: 'created_by'

  validates :first_name, :last_name, :password, presence: true
  validates :email, presence: true, uniqueness: { case_sensitive: false }, format: { with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/ }

  def task_by_id(task_id)
    tasks.find(task_id)
  end
end
