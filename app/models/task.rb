class Task < ApplicationRecord
  enum status: { queued: 0, completed: 1 }
  enum priority: { high: 2, medium: 1, low: 0 }

  belongs_to :created_by, class_name: 'User', foreign_key: 'user_id'

  validates :title, presence: true

  scope :by_priority, ->{ order(:priority).reverse_order }
  scope :by_status, ->(status){ where(status: status) }

  def mark_as_completed
    update(status: 'completed')
  end
end
