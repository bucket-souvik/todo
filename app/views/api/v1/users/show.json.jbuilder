json.user do
  json.extract! @user, :first_name, :last_name, :email
end
