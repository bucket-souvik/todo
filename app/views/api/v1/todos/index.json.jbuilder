json.set! :todos do
  json.partial! 'todo', collection: @tasks, as: :task
end
