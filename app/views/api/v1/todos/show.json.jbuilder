json.todo do
  json.partial! 'todo', locals: { task: @task }
end
