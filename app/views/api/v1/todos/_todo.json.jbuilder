json.extract! task, :title, :description, :status, :priority, :created_at
json.created_by do
  json.extract! task.created_by, :first_name, :last_name, :email
end
