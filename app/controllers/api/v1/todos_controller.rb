class Api::V1::TodosController < ApplicationController
  before_action :authenticate_user

  def index
    @tasks = current_user.tasks.by_priority
  end

  def create
    @task = Task.new(todo_params.merge(created_by: current_user))
    if @task.save!
      render :show, status: :created
    end
  end

  def show
    @task = current_user.task_by_id(params[:id])
  end

  def update
    @task = current_user.task_by_id(params[:id])
    if @task.update!(todo_params)
      render :show, status: :ok
    end
  end

  def destroy
    @task = current_user.task_by_id(params[:id])
    @task.destroy
    head :ok
  end

  def done
    @task = current_user.task_by_id(params[:id])
    if @task.mark_as_completed
      render :show, status: :ok
    else
      render json: { error: {messages: ['Unable to mark as completed']} }, status: :unprocessable_entity
    end
  end

  private
  def todo_params
    params.require(:todo).permit(:title, :description, :priority)
  end
end
