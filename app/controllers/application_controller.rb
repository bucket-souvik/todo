class ApplicationController < ActionController::API
  include Knock::Authenticable

  rescue_from ActiveRecord::RecordInvalid, with: :handle_record_invalid_error
  rescue_from ActiveRecord::RecordNotFound, with: :handle_record_not_found_error

  private
  def handle_record_invalid_error(exception)
    errors = exception.record.errors
    render json: { error: { messages: errors.full_messages } }, status: :unprocessable_entity
  end

  def handle_record_not_found_error(exception)
    render json: { error: { messages: [exception.message] } }, status: :unprocessable_entity
  end
end
