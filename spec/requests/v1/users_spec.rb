require 'rails_helper'

RSpec.describe 'Users API', type: :request do
  describe 'create' do
    let(:user_params) do
      { first_name: 'some-name', last_name: 'surname', email: 'user@example.com', password: '123' }
    end

    it 'saves users with first name, last name, email and password' do
      expect do
        post '/api/users', params: { user: user_params }
      end.to change{ User.count }.by(1)
    end

    it 'sends user detail in repose' do
      post '/api/users', params: { user: user_params }

      user_response = JSON.parse(response.body)
      expect(user_response).to include({'user' => {'firstName' => 'some-name', 'lastName' => 'surname', 'email' => 'user@example.com'}})
    end
  end
end
