require 'rails_helper'

RSpec.describe 'Todos API', type: :request do
  let(:task_params) do
    { title: 'some-title', description: 'some-description' }
  end
  let(:logged_user){ create(:user) }

  describe 'list' do
    it 'send all tasks of logged-in user' do
      create_list(:task, 10, { created_by: logged_user })
      get '/api/todos', headers: authenticated_headers(logged_user)

      list = JSON.parse(response.body)
      expect(response).to be_success
      expect(list['todos'].length).to eq(10)
    end

    it 'should not send tasks of non logged-in user' do
      create_list(:task, 2, { created_by: create(:user) })
      get '/api/todos', headers: authenticated_headers(logged_user)

      list = JSON.parse(response.body)
      expect(list['todos'].length).not_to eq(2)
    end
  end

  describe 'create' do
    it 'saves task with default status' do
      post '/api/todos', params: { todo: task_params }, headers: authenticated_headers(logged_user)

      created_task = JSON.parse(response.body)
      expect(created_task['todo']['status']).to eq('queued')
    end

    it 'saves task with default priority' do
      post '/api/todos', params: { todo: task_params }, headers: authenticated_headers(logged_user)

      created_task = JSON.parse(response.body)
      expect(created_task['todo']['priority']).to eq('low')
    end

    it 'can create task with specific priority' do
      post '/api/todos', params: { todo: task_params.merge(priority: 'medium') }, headers: authenticated_headers(logged_user)

      created_task = JSON.parse(response.body)
      expect(created_task['todo']['priority']).to eq('medium')
    end
  end

  describe 'update' do
    it 'can change attribute of task' do
      task = create(:task, { created_by: logged_user })
      expect do
        put "/api/todos/#{task.id}", params: { todo: {priority: 'medium'} }, headers: authenticated_headers(logged_user)
      end.to change{ Task.first.priority }.from('low').to('medium')
    end
  end

  describe 'destroy' do
    it 'deletes task' do
      create_list(:task, 10, { created_by: logged_user })
      task = Task.last
      expect do
        delete "/api/todos/#{task.id}", headers: authenticated_headers(logged_user)
      end.to change(Task, :count).from(10).to(9)
    end
  end

  describe 'done' do
    it 'change task status as completed' do
      task = create(:task, { created_by: logged_user })
      expect do
        post "/api/todos/#{task.id}/done", headers: authenticated_headers(logged_user)
      end.to change{ Task.first.status }.from('queued').to('completed')
    end
  end
end
