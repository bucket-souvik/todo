module AuthenticationHelper
  def authenticated_headers(user, version=1)
    token = Knock::AuthToken.new(payload: { sub: user.id }).token
    {
      "Accept" => "application/api.v#{version}",
      "Authorization" => "Bearer #{token}"
    }
  end
end
