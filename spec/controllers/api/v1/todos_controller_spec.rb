require 'rails_helper'

RSpec.describe Api::V1::TodosController, type: :controller do
  let(:task_params){ {title: 'some-title', description: 'some-description'} }
  let(:task){ double('task') }
  let(:logged_user){ create(:user) }

  before(:example) do
    authenticated_headers(logged_user).each do |head_key, value|
      request.headers[head_key] = value
    end
  end

  describe 'GET #index' do
    it 'finds all tasks' do
      expect_any_instance_of(User).to receive_message_chain(:tasks, :by_priority)
      get :index, format: 'json'
    end

    it 'sends OK reponse' do
      get :index, format: :json
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    it 'creates new task' do
      allow(task).to receive(:save!){ true }
      expect(Task).to receive(:new).and_return(task)
      post :create, format: :json, params: { todo: task_params }
    end

    it 'saves new task' do
      allow(Task).to receive(:new) { task }
      expect(task).to receive(:save!).and_return(true)
      post :create, format: :json, params: { todo: task_params }
    end

    it 'sends created response' do
      post :create, format: :json, params: { todo: task_params }
      expect(response).to have_http_status(:created)
    end

    it 'raises error with invalid data' do
      bypass_rescue
      expect do
        post :create, format: :json, params: { todo: {description: 'some-description'} }
      end.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  describe 'GET #show' do
    it 'finds task by id' do
      expect_any_instance_of(User).to receive(:task_by_id)
      get :show, format: :json, params: { id: 'some-id' }
    end
  end

  describe 'PUT #update' do
    let(:task_frm_db){ create(:task) }

    it 'finds task by id' do
      allow(task).to receive(:update!)
      expect_any_instance_of(User).to receive(:task_by_id).and_return(task)
      put :update, format: :json, params: { id: 'some-id', todo: task_params }
    end

    it 'updates task' do
      allow_any_instance_of(User).to receive(:task_by_id).and_return(task)
      expect(task).to receive(:update!)
      put :update, format: :json, params: { id: 'some-id', todo: task_params }
    end

    it 'raises error with invalid data id' do
      bypass_rescue
      expect do
        put :update, format: :json, params: { id: 'some-invalid-id', todo: task_params }
      end.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'sends response with ok status on successful update' do
      allow_any_instance_of(User).to receive(:task_by_id).and_return(task)
      allow(task).to receive(:update!).and_return(true)
      put :update, format: :json, params: { id: 'some-id', todo: task_params }
      expect(response).to have_http_status(:ok)
    end

    it 'raises error on invalid parameters data' do
      bypass_rescue
      allow_any_instance_of(User).to receive(:task_by_id).and_return(task_frm_db)
      expect do
        put :update, format: :json, params: { id: 'some-id', todo: {title: ''} }
      end.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  describe 'POST #done' do
    it 'finds task by id' do
      allow(task).to receive(:mark_as_completed)
      expect_any_instance_of(User).to receive(:task_by_id).and_return(task)
      post :done, format: :json, params: { id: 'some-id' }
    end

    it 'raises error with invalid data id' do
      bypass_rescue
      expect do
        post :done, format: :json, params: { id: 'some-invalid-id' }
      end.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'calls update status as completed' do
      allow_any_instance_of(User).to receive(:task_by_id).and_return(task)
      expect(task).to receive(:mark_as_completed)
      post :done, format: :json, params: { id: 'some-id' }
    end

    it 'sends response OK' do
      allow_any_instance_of(User).to receive(:task_by_id).and_return(task)
      allow(task).to receive(:mark_as_completed).and_return(true)
      post :done, format: :json, params: { id: 'some-id' }
      expect(response).to have_http_status(:ok)
    end

    it 'sends error response when unable to mark complete' do
      allow_any_instance_of(User).to receive(:task_by_id).and_return(task)
      allow(task).to receive(:mark_as_completed).and_return(false)
      post :done, format: :json, params: { id: 'some-id' }
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  describe 'DELETE #destroy' do
    it 'finds task by id' do
      allow(task).to receive(:destroy)
      expect_any_instance_of(User).to receive(:task_by_id).and_return(task)
      delete :destroy, format: :json, params: { id: 'some-id' }
    end

    it 'sends OK status on success' do
      allow_any_instance_of(User).to receive(:task_by_id).and_return(task)
      allow(task).to receive(:destroy){ allow(task).to receive(:frozen?).and_return(true) }
      delete :destroy, format: :json, params: { id: 'some-id' }
      expect(response).to have_http_status(:ok)
    end
  end
end
