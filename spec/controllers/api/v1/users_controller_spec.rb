require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  before(:each) do
    request.headers['Accept'] = 'application/api.v1'
    request.headers['Content-Type'] = 'application/json'
  end

  describe 'POST #create' do
    let(:user_params) do
      { first_name: 'some-name', last_name: 'surname', email: 'user@example.com', password: '123' }
    end
    let(:user){ double('user') }

    it 'creates new user' do
      allow(user).to receive(:save!)
      expect(User).to receive(:new).and_return(user)
      post :create, format: :json, params: { user: user_params }
    end

    it 'saves new user' do
      allow(User).to receive(:new).and_return(user)
      expect(user).to receive(:save!)
      post :create, format: :json, params: { user: user_params }
    end

    it 'sends created response' do
      post :create, format: :json, params: { user: user_params }
      expect(response).to have_http_status(:created)
    end

    it 'raises error with invalid data' do
      bypass_rescue
      expect do
        post :create, format: :json, params: { user: {last_name: 'some-last-name'} }
      end.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
