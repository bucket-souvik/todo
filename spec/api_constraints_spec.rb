require 'rails_helper'

RSpec.describe ApiConstraints, type: :lib do
  describe '#matches' do
    let(:version) { 'some-version-number' }
    let(:req) { OpenStruct.new(headers: {'Accept' => "application/api.v#{version}"}) }

    it 'returns true for default version' do
      constraint = ApiConstraints.new(version: version, default: true)
      expect(constraint).to be_matches(req)
    end

    it 'returns false for unspported version' do
      constraint = ApiConstraints.new(version: 'wrong-version')
      expect(constraint).not_to be_matches(req)
    end

    it 'returns true for supported version' do
      constraint = ApiConstraints.new(version: version)
      expect(constraint).to be_matches(req)
    end
  end
end
