require 'rails_helper'

RSpec.describe Task, type: :model do
  subject{ described_class.new }

  it 'is not valid without title' do
    expect(subject).not_to be_valid
  end

  it 'is not valid without created by' do
    subject.title = 'some-title'
    expect(subject).not_to be_valid
  end

  it 'is valid with title and created by' do
    user = create(:user)
    subject.title = 'some-title'
    subject.created_by = user
    expect(subject).to be_valid
  end

  describe '#mark_as_completed' do
    it 'updates status as completed' do
      task = create(:task)
      expect{ task.mark_as_completed }.to change{ task.status }.from('queued').to('completed')
    end
  end
end
