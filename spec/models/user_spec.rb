require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'task by id' do
    let(:user){ create(:user_with_tasks, task_count: 2) }

    it 'returns specifec task matching with id' do
      one_task = user.tasks.first
      expect(user.task_by_id(one_task.id)).to eq(one_task)
    end
  end
end
