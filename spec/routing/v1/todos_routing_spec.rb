require 'rails_helper'

RSpec.describe 'routes for Todos', type: :routing do
  it 'routes /api/todos to the Todos controller' do
    expect(get('/api/todos')).to route_to('api/v1/todos#index', format: 'json')
  end

  it 'routes /api/todos/some-id to the Todos controller and show action' do
    expect(get('/api/todos/some-id')).to route_to('api/v1/todos#show', id: 'some-id', format: 'json')
  end

  it 'routes /api/todos to the Todos controller and create action via post' do
    expect(post('api/todos')).to route_to('api/v1/todos#create', format: 'json')
  end

  it 'routes /api/todos/some-id to the Todos controller and update action via put' do
    expect(put('api/todos/some-id')).to route_to('api/v1/todos#update', id: 'some-id', format: 'json')
  end

  it 'routes /api/todos/some-id to the Todos controller and destroy action via delete' do
    expect(delete('api/todos/some-id')).to route_to('api/v1/todos#destroy', id: 'some-id', format: 'json')
  end

  it 'routes /api/todos/some-id/done to the Todos controller and done action via post' do
    expect(post('api/todos/some-id/done')).to route_to('api/v1/todos#done', id: 'some-id', format: 'json')
  end
end
