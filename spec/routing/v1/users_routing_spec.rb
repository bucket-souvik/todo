require 'rails_helper'

RSpec.describe 'routes for Users', type: :routing do
  it 'routes /api/users to the Users controller' do
    expect(post('/api/users')).to route_to('api/v1/users#create', format: 'json')
  end

  it 'routes /api/user_token to the UserToken controller' do
    expect(post('/api/user_token')).to route_to('api/v1/user_token#create', format: 'json')
  end
end
