FactoryBot.define do
  factory :user do
    sequence(:first_name){ |n| "Firstname#{n}" }
    sequence(:last_name){ |n| "Lastname#{n}" }
    email { "#{first_name}.#{last_name}@example.com".downcase }
    password 'password'

    factory :user_with_tasks do
      transient do
        task_count 5
      end

      after(:create) do |user, evaluator|
        create_list(:task, evaluator.task_count, created_by: user)
      end
    end
  end
end
