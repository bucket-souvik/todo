FactoryBot.define do
  factory :task do
    sequence(:title){ |n| "Task Title #{n}" }
    sequence(:description){ |n| "Task Description #{n}" }
    priority :low
    status :queued
    association :created_by, factory: :user

    factory :low_priority_task do
      priority :low
    end

    factory :medium_priority_task do
      priority :medium
    end

    factory :high_priority_task do
      priority :low
    end
  end
end
