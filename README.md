# TODO API

Create your daily Todos and mark them as completed one by one. You can update your Todos by changing its name, description or by changing its priority.

# Dependencies
- Ruby 2.4.3
- Rails 5.1.5
- Database: PostgreSQL 10

# Application Setup

Clone the source code from [Bitbucket](https://bitbucket.org) using below command:
```
git clone git@bitbucket.org:bucket-souvik/todo.git
```
Install all gem dependencies using bundler:
```
cd todo
bin/bundle install
```

Setup database using rake task:
```
bin/rails db:setup
```

Start server:
```
bin/rails server
```

# API Details

## To create user
```
curl -X POST https://skhan-todo-api.herokuapp.com/api/users -H 'Accept: application/api.v1' -H 'Content-Type: application/json' -d '{"user":{"first_name":"Souvik","last_name":"Khan","email":"souvik.khan@example.com","password":"abc123"}}'
```
Output:
```
{"user":{"firstName":"Souvik","lastName":"Khan","email":"souvik.khan@example.com"}}
```

## To get authorization token:
```
curl -X POST https://skhan-todo-api.herokuapp.com/api/user_token -H 'Accept: application/api.v1' -H 'Content-Type: application/json' -d '{"auth":{"email":"souvik.khan@example.com","password":"<PASSWORD>"}}'
```
Output:
```
{"jwt":"<AUTH-TOKEN>"}
```

## To create Todo:
```
curl -X POST https://skhan-todo-api.herokuapp.com/api/todos -H 'Accept: application/api.v1' -H 'Content-Type: application/json' -H 'Authorization: Bearer <AUTH-TOKEN>' -d '{"todo":{"title": "Some Title","description": "Some Description","priority": "high"}}'
```

## To show Todo's detail:
```
curl -X GET https://skhan-todo-api.herokuapp.com/api/todos/:todo_id -H 'Accept: application/api.v1' -H 'Content-Type: application/json' -H 'Authorization: Bearer <AUTH-Token>'
```

## To update Todo's detail:
```
curl -X PUT https://skhan-todo-api.herokuapp.com/api/todos/:todo_id -H 'Accept: application/api.v1' -H 'Content-Type: application/json' -H 'Authorization: Bearer <AUTH-TOKEN>' -d '{"todo":{"priority":"medium"}}'
```

## To mark Todo as completed:
```
curl -X POST https://skhan-todo-api.herokuapp.com/api/todos/:todo_id/done -H 'Accept: application/api.v1' -H 'Content-Type: application/json' -H 'Authorization: Bearer <AUTH-TOKEN>'
```

## To delete a Todo:
```
curl -X DELETE https://skhan-todo-api.herokuapp.com/api/todos/:todo_id -H 'Accept: application/api.v1' -H 'Content-Type: application/json' -H 'Authorization: Bearer <AUTH-TOKEN>'
```
